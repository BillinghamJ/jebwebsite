package main

import (
	"github.com/hoisie/web"
)

func sanityTest() string {
	return "Hello, world!"
}

func main() {
	web.Get("/", sanityTest)
	web.Run("0.0.0.0:9999")
}
